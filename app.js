var app = angular.module('app', ['ngRoute']);

app.config(['$routeProvider', function($routeProvider) {

    $routeProvider
        .when("/", {
            templateUrl: "nombreSociedad.html",
            controller: "appControlador"
        })
        .when("/juridicas", {
            templateUrl: "juridicas.html",
            controller: "appControlador"
        })
        .when("/naturales", {
            templateUrl: "naturales.html",
            controller: "appControlador"
        })
        .when("/domicilioSociedad", {
            templateUrl: "domicilioSociedad.html",
            controller: "appControlador"
        })
        .when("/objeto", {
            templateUrl: "objeto.html",
            controller: "appControlador"
        })
        .when("/duracion", {
            templateUrl: "duracion.html",
            controller: "appControlador"
        }).when("/capital", {
            templateUrl: "capital.html",
            controller: "appControlador"
        }).when("/composicionAccionaria", {
            templateUrl: "composicionAccionaria.html",
            controller: "appControlador"
        }).when("/derechosAccionistas", {
            templateUrl: "derechosAccionistas.html",
            controller: "appControlador"
        }).when("/dias", {
            templateUrl: "dias.html",
            controller: "appControlador"
        }).when("/funcionesAsamblea", {
            templateUrl: "funcionesAsamblea.html",
            controller: "appControlador"
        }).when("/reservaLegal", {
            templateUrl: "reservaLegal.html",
            controller: "appControlador"
        }).when("/nombramientos", {
            templateUrl: "nombramientos.html",
            controller: "appControlador"
        }).when("/controversias", {
            templateUrl: "controversias.html",
            controller: "appControlador"
        })


}]);

app.controller("appControlador", ["$scope", function($scope) {
    var cont = this;
    cont.mensaje = "mensaje";
    cont.arr = ["arr1", "arr2", "arr3"];

    cont.numeros = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"];
    cont.letras = ["o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "aa", "ab", "ac", "ad", "ae", "af", "ag", "ah"];
    // Persona juridica toma de datos
    cont.razonSocial = [];
    cont.nit = [];
    cont.numeroMatricula = [];
    cont.fechaMatricula = [];
    cont.lugarMatricula = [];
    // Persona natural toma de datos
    cont.nombres = [];
    cont.apellidos = [];
    cont.tipodoc = [];
    cont.numeroid = [];
    cont.fechaExpedicion = [];
    cont.lugarExpedicion = [];
    //Objetos CIIU
    cont.descripcionCIIU = [];
    cont.ciiu = [];
    cont.sumaAccionesSuscritasTabla = "0";
    //Inicialización derecho de preferencia en NO
    cont.textoDerechoPreferencia = "En el evento en que la sociedad adquiera mas accionistas, los accionistas que pretendan vender parte o la totalidad de las acciones, deberán ofrecerlas a los accionistas y/o terceros, dentro de los quince (15) días hábiles siguientes, los interesados deberán manifestar si tienen interés en adquirirlas. El precio, plazo y demás condiciones de la cesión se expresarán en la oferta. "

    $scope.sumatoriaAccionesSuscritas = function(index) {
        var total = 0;
        var control = index
            // for (var i = 0; i <= cont.numeronumeroNaturales + cont.numerojuridicas; i++) {
            //     total = cont.accionesSuscritasTabla[i] + total
            // }
        for (var i = 0; i <= control; i++) {
            total += $scope.cont.accionesSuscritasTabla[i]
        }
        $scope.cont.sumaAccionesSuscritasTabla = total
        return total;
    }
    $scope.sumatoriaAccionesPagadas = function(index) {
        var total = 0;
        var control = index

        for (var i = 0; i <= control; i++) {
            total += $scope.cont.accionesPagadasTabla[i]
        }
        $scope.cont.sumaAccionesPagadasTabla = total
        return total;
    }
    $scope.sumatoriaCapitalTotal = function(index) {
        var total = 0;
        var control = index

        for (var i = 0; i <= control; i++) {
            total += $scope.cont.capitalTotalTabla[i]
        }
        $scope.cont.sumaCapitalTotal = total
        return total;
    }
    $scope.derechodePreferencia = function() {
        var eleccion = $scope.cont.derechoPreferencia
        if (eleccion != "") {
            $scope.cont.textoDerechoPreferencia = "En el evento en que la sociedad adquiera mas accionistas, los accionistas que pretendan vender parte o la totalidad de las acciones, deberán ofrecerlas preferentemente a los demás accionistas de conformidad con cualquiera de las siguientes opciones: (i) oferta de acciones en Asamblea de Accionistas, (ii) carta de oferta de acciones dirigida a cada uno de los accionistas, o (iii) por conducto del representante legal de la sociedad, quien deberá dar traslado inmediatamente a los demás accionistas. <br> Posteriormente a la oferta de acciones, dentro de los quince (15) días hábiles siguientes, los demás accionistas deberán manifestar si tienen interés en adquirirlas. Transcurrido este lapso, los accionistas que acepten la oferta tendrán derecho a tomarla a prorrata de las acciones que posean. El precio, plazo y demás condiciones de la cesión se expresarán en la oferta. Si ningún accionista manifiesta interés en adquirir las acciones dentro del término antes mencionado, el cedente quedará en libertad de ofrecerlas a cualquier tercero, quien deberá cumplir con los mismos términos de la oferta inicial. "
        }

        return null;
    }

}]);

function generarPDF() {
    const element = document.getElementById("documento");
    var opt = {
        margin: 3,
        filename: 'ConstitucionDeSociedad.pdf',
        jsPDF: {
            unit: 'cm',
            format: 'legal',
            orientation: 'portrait'
        }
    };
    html2pdf().set(opt, {
        pagebreak: { mode: 'avoid-all' }
    })

    .from(element)
        .save();
};